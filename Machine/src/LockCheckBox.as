package  
{
	import Box2D.Common.Math.b2Vec2;
	import flash.geom.Point;
	import org.flixel.*;
	
	/**
	 * ...
	 * @author Ian Guthridge
	 */
	public class LockCheckBox extends FlxSprite 
	{
		public var locked:Boolean;
		public function LockCheckBox(X:Number, Y:Number) 
		{
			super(X, Y, null);
			this.loadGraphic(AssetsRegistry.checkboxEmptyPNG);
			//this.makeGraphic(10 , 10, 0xffff0000);
			locked = true;
		}
		
		public function onMouseDown():void
		{
			if (this.pixelsOverlapPoint(FlxG.mouse))
			{
				locked = !locked;
				if (locked)
				{
					//this.makeGraphic(10 , 10, 0xffff0000);
					this.loadGraphic(AssetsRegistry.checkboxEmptyPNG);
				}
				else
				{
					//this.makeGraphic(10 , 10, 0xff0000ff);
					this.loadGraphic(AssetsRegistry.checkboxFullPNG);
				}
			}
		}
			
	}

}