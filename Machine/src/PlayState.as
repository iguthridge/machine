package
{

	import Box2D.Common.Math.b2Vec2;
	import Box2D.Dynamics.b2DebugDraw;
	import Box2D.Dynamics.b2World;
	import flash.display.Sprite;
	import org.flixel.*;
	import org.flixel.plugin.photonstorm.FlxButtonPlus;
	import items.Goal;
	
	public class PlayState extends FlxState
	{
		public var world:b2World;
		protected var m_items:FlxGroup;
		protected var m_unplaced:Array;
		protected var debug:b2DebugDraw;
		protected var m_resetButton:FlxButtonPlus;
		protected var m_saveButton:FlxButtonPlus;
		protected var m_title:FlxText;
		protected var m_recording = false;
		
		protected var m_initialItems:Array;
		protected var m_testMode:Boolean
		protected var m_fromInv:Array;
		
		//public var m_replayItems:Array;
		
		public function PlayState(items:Array, initialItems:Array, fromInv:Array, testMode:Boolean)
		{
			/*if (items == null)
			{
				items = m_replayItems;
			}
			else
			{
				m_replayItems = initialItems;
			}*/
			
			m_title = new FlxText(FlxG.stage.stageWidth / 2 - 80, 15, 200, "Play Mode")
			m_title.alignment = "center";
			m_title.size = 14;
			add(m_title);
			
			m_resetButton = new FlxButtonPlus(FlxG.stage.stageWidth / 2 - 80, 50, onReset, null, "Reset Machine", 150, 50);
			add(m_resetButton);
			
			m_unplaced = items;
			m_initialItems = initialItems;
			m_fromInv = fromInv;
			m_testMode = testMode;
			debug = new b2DebugDraw();
			var s:Sprite = new Sprite();
			FlxG.stage.addChild(s);
			debug.SetSprite(s);
			debug.SetDrawScale(1);// 30.0;
			debug.SetFillAlpha(0.3);
			debug.SetLineThickness(1.0);
			debug.SetFlags(b2DebugDraw.e_shapeBit | b2DebugDraw.e_jointBit);
		}
		
		override public function create():void
		{
			world = new b2World(new b2Vec2(0, 98.1), true);
			world.SetContactListener(new ContactListener());
			//world.SetDebugDraw(debug);
			m_items = new FlxGroup();
			placeItems(m_unplaced);
			m_unplaced = null;
		}
				
		public function placeItems(items:Array):void
		{
			for each(var item:Item in items)
			{
				item.buildPhysics(world);
				m_items.add(item);
				if (item is Goal)
				{
					(item as Goal).completeCallback = complete;
				}
			}
			this.add(m_items);
		}
		
		public function complete():void
		{
			
			FlxG.switchState(new VictoryState(m_testMode, m_initialItems, m_fromInv));
		}
		
		override public function update():void
		{
			var elapsed:Number = FlxG.elapsed;
			super.update();
			m_items.update();
			world.Step(elapsed, 10, 10);
		/*if(!m_recording){
				start_record();
			}*/
		}
		
		/*private function start_record():void {
			m_recording = true;
			FlxG.recordReplay(false);
		}

		public static var save:String;		*/
		
		public function onReset():void
		{
			//m_recording = false;

			//save = FlxG.stopRecording();
			
			FlxG.switchState(new ConstructState(m_testMode, m_initialItems, m_fromInv));
			
		}
	}
}

