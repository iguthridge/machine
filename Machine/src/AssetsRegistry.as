package  
{
	/**
	 * All item assets.
	 * @author Anders Howard
	 */
	public class AssetsRegistry 
	{
		//Item sprites.
		[Embed(source = 'assets/arrow01_16px.png')] public static var rotatorArrowPNG:Class;
		[Embed(source = 'assets/ballTennis02_32px.png')] public static var tennisBallPNG:Class;
		[Embed(source = 'assets/brickTile01.png')] public static var brickWallPNG:Class;
		[Embed(source = 'assets/dominoTile04.png')] public static var dominoPNG:Class;
		[Embed(source = 'assets/goalButton01.png')] public static var goalButtonPNG:Class;
		[Embed(source = 'assets/flintStrip01.png')] public static var flintSheetPNG:Class;
		[Embed(source = 'assets/flint_frame01.png')] public static var flintUnlitPNG:Class;
		[Embed(source = 'assets/flint_ignited01.png')] public static var flintLitPNG:Class;
		[Embed(source = 'assets/rocketLitState01.png')] public static var rocketLitPNG:Class;
		[Embed(source = 'assets/rocketUnlitState01.png')] public static var rocketUnlitPNG:Class;
		[Embed(source = 'assets/checkboxEmpty01.png')] public static var checkboxEmptyPNG:Class;
		[Embed(source = 'assets/checkboxEmpty02.png')] public static var checkboxFullPNG:Class;
		[Embed(source = 'assets/winnerDialog01.png')] public static var winnerDialogPNG:Class;
		
		public function AssetsRegistry() 
		{
		}
		
	}

}