package  
{
	import flash.events.MouseEvent;
	import mx.core.FlexSprite;
	import org.flixel.*;
	import org.flixel.FlxG;
	import org.flixel.FlxSprite;
	/**
	 * Toybox is the interface that holds all of the toys.
	 * Takes a mode (construction or play mode) in its constructor that later decides which kind of inventory it will take.
	 * @author Anders Howard
	 */
	public class Toybox extends FlxGroup
	{
		private var m_currentToysArray:Vector.<String>;
		private var m_currentMode:String;
		private var m_currentLevel:uint;
		private var m_inventories:InventoryManager;
		private var m_sprites:Array;
		private var m_selected:FlxSprite;
		private var m_initialPos:FlxPoint;
		private var m_mouseOffset:FlxPoint;
		
		static const MODE_CONSTRUCTION:String = "mode_construction";
		static const MODE_LEVEL:String = "mode_level";
		
		private const TOYBOX_HEIGHT:uint = FlxG.stage.stageHeight * 0.15;
		private const TOYBOX_WIDTH:uint = FlxG.stage.stageWidth;
		private const TOYBOX_TOYCOUNT:uint = 6;
		private const TOY_SPACING:uint = 100;
		
		private var m_toyBoxBackdrop:FlxSprite;
		private var m_used:Array;
		private var m_parentState:ConstructState;
		
		public function Toybox(mode:String, inventory:InventoryManager, parentState:ConstructState, used:Array) 
		{
			m_parentState = parentState;
			
			m_used = used;
			//Make sure the Toybox knows what mode we're in.
			if (mode != MODE_LEVEL)
			{
				if (mode != MODE_CONSTRUCTION)
				{
					throw new Error("Toybox requires MODE_CONSTRUCTION or MODE_PLAY.");
				}
			}
			m_currentMode = mode;
			trace("Toybox mode is " + m_currentMode);
			
			m_inventories = inventory;
			
			//Set up Toybox sprite.
			m_toyBoxBackdrop = new FlxSprite(0, FlxG.stage.stageHeight - TOYBOX_HEIGHT);
			m_toyBoxBackdrop.makeGraphic(TOYBOX_WIDTH, TOYBOX_HEIGHT, 0xff889966, true);
			add(m_toyBoxBackdrop);
			loadInventory();
		}
		
		//Load inventory images based on mode here.
		private function loadInventory():void
		{
			if (m_currentMode == "mode_level")
			{
				m_currentToysArray = m_inventories.getLevelInventory(1);
			}
			else if (m_currentMode == "mode_construction")
			{
				m_currentToysArray = m_inventories.getPlayerInventory();
			}
			setupItemDisplay();
			FlxG.stage.addEventListener(MouseEvent.MOUSE_DOWN, onMouseDown);
			FlxG.stage.addEventListener(MouseEvent.MOUSE_UP, onMouseUp);
		}
		
		public function removeEventListeners():void
		{
			FlxG.stage.removeEventListener(MouseEvent.MOUSE_DOWN, onMouseDown);
			FlxG.stage.removeEventListener(MouseEvent.MOUSE_UP, onMouseUp);
		}
		
		//Display the actual toy sprites here.
		private function setupItemDisplay():void
		{	
			m_sprites = [];
			for (var i:int = 0; i < m_currentToysArray.length; i++)
			{
				var index:int = m_used.indexOf(m_currentToysArray[i]);
				if (index < 0 || m_currentMode == MODE_CONSTRUCTION)
				{
					createItemSlot(i);
				}
				else
				{
					m_currentToysArray.splice(i, 1);
					m_used.splice(index, 1);
					--i;
				}
			}
		}
		
		
		override public function update():void 
		{
			super.update();
			if (m_selected != null)
			{
				var dx:Number = (FlxG.mouse.x - m_mouseOffset.x) - m_selected.x;
				var dy:Number = (FlxG.mouse.y - m_mouseOffset.y) - m_selected.y;
				m_selected.x += dx;
				m_selected.y += dy;
			}
		}
		
		private function onMouseDown(event:MouseEvent):void
		{
			for each(var sprite:FlxSprite in m_sprites)
			{
				if (sprite.pixelsOverlapPoint(FlxG.mouse))
				{
					m_selected = sprite;
					m_initialPos = new FlxPoint(sprite.x, sprite.y);
					m_mouseOffset  = new FlxPoint(FlxG.mouse.x - sprite.x, FlxG.mouse.y - sprite.y);
					break;
				}
			}
		}		
		
		private function onMouseUp(event:MouseEvent):void
		{
			if (m_selected != null)
			{
				if (m_selected.y + m_selected.height < m_toyBoxBackdrop.y)
				{
					var i:int = m_sprites.indexOf(m_selected);
					m_parentState.place(Item.createItem(m_currentToysArray[i], m_selected.x, m_selected.y));
					if (m_currentMode != MODE_CONSTRUCTION)
					{
						remove(m_selected);
						m_currentToysArray.splice(i, 1);
						m_sprites.splice(i, 1);
						for (i; i < m_sprites.length; ++i)
						{
							var sprite:FlxSprite = m_sprites[i];
							var pos:FlxPoint = new FlxPoint(sprite.x, sprite.y);
							sprite.x = m_initialPos.x;
							sprite.y = m_initialPos.y;
							m_initialPos = pos;
							
						}
					}
					else
					{
						m_selected.x = m_initialPos.x;
						m_selected.y = m_initialPos.y;
					}
				}
				else
				{
					m_selected.x = m_initialPos.x;
					m_selected.y = m_initialPos.y;
				}				
				m_selected = null;
				m_selected = null;
			}
		}
		
		private function createItemSlot(index:int):void
		{
			var xLoc:int;
			if (index == 0)
			{
				xLoc = TOY_SPACING/2;
			}
			else {
				xLoc = (TOY_SPACING/2 + TOY_SPACING * index);
			}
			
			var spotSprite:FlxSprite = new FlxSprite(xLoc, (FlxG.stage.stageHeight - TOYBOX_HEIGHT / 1.5), Item.getAsset(m_currentToysArray[index]));
			m_sprites.push(spotSprite);
			add(spotSprite);
		}
		
	}

}