package
{

	import org.flixel.*;
	import items.*;
	import items.Ball;

	public class ConstructState extends FlxState
	{
		private var playButton:FlxButton;
		private var Title:FlxText;
		
		private var m_items:Array;
		
		protected var m_hudGroup:FlxGroup;
		protected var m_toybox:Toybox;
		protected var m_inventoryManager:InventoryManager;
		protected var m_isConstruction:Boolean;
		protected var m_placed:Array;
		public function ConstructState(isConstruction:Boolean, items:Array = null, placed:Array = null)
		{
			m_isConstruction = isConstruction;
			m_items = items;
			m_placed = placed;
			if (!m_placed)
			{
				m_placed = [];
			}
		}
		
		override public function create():void
		{
			//Load toybox
			m_inventoryManager = new InventoryManager;
			trace("Adding toybox.");
			var copy:Array = m_placed.slice(0, m_placed.length);
			m_toybox = new Toybox(m_isConstruction ? Toybox.MODE_CONSTRUCTION : Toybox.MODE_LEVEL, m_inventoryManager, this, copy);
			add(m_toybox);
						
			FlxG.bgColor = 0xff111122;
			
			Title = new FlxText(FlxG.stage.stageWidth / 2 - 80, 15, 200, "Construction Mode")
			Title.alignment = "center";
			Title.size = 14;
			add(Title);
			
			playButton = new FlxButton(FlxG.width/2-20,FlxG.height + 100, "Run Machine!", onPlay);
			playButton.soundOver = null;  //replace with mouseOver sound
			playButton.color = 0xff59F905;
			playButton.label.color = 0xff000000;
			playButton.y = 400;
			add(playButton);
			
			
			if (m_items != null)
			{
				for each(var item:Item in m_items)
				{
					item.active = true;
				}
			}
			else
			{
			
				m_items = new Array();		
				if (LevelSelectState.currentWorld)
				{
					var levelData:Array = LevelSelectState.currentWorld;
					for each(var item:Item in levelData)
					{	
						var clone:Item = Item.cloneItem(item);
						if (m_isConstruction)
						{
							clone.addListeners();
						}
						m_items.push(clone);
					}
				}
				else
				{
					m_items.push(Item.createItem("goal", 400, 100, m_isConstruction));
				}
				
			}
			

			for each(var item:Item in m_items)
			{
				add(item);
			}
			
			FlxG.mouse.show();
			
		}
		
		override public function update():void
		{
			super.update();
			//FlxG.visualDebug = true;
			for each(var item:Item in m_items)
			{
				item.update();
			}
			if(FlxG.keys.justPressed("S"))
			{
				var xml:XML = <world/>;
				for each(var item:Item in m_items)
				{
					var xmlItem:XML = <item/>;
					xmlItem.@name = item.name;
					xmlItem.@x = item.x;
					xmlItem.@y = item.y;
					xmlItem.@angle = item.angle;
					xml.appendChild(xmlItem);
				}
				trace(xml.toXMLString());
			}
		}
		
		public function place(item:Item):void 
		{
			m_placed.push(item.name);
			m_items.push(item);
			add(item);
		}
				
		protected function onPlay():void
		{
			playButton.exists = false;
			var simItems:Array = [];
			for each(var item:Item in m_items)
			{
				var clone:Item = Item.cloneItem(item);
				simItems.push(clone);
				item.active = false;
				remove(item);
			}
			m_toybox.removeEventListeners();
			FlxG.switchState(new PlayState(simItems, m_items, m_placed, m_isConstruction));
		}
	}
}

