package  
{
	import Box2D.Collision.Shapes.b2CircleShape;
	import Box2D.Common.Math.b2Vec2;
	import Box2D.Dynamics.b2Body;
	import Box2D.Dynamics.b2BodyDef;
	import Box2D.Dynamics.b2Fixture;
	import Box2D.Dynamics.b2FixtureDef;
	import Box2D.Dynamics.b2World;
	import flash.events.MouseEvent;
	import flash.geom.Point;
	import flash.utils.ByteArray;
	import flash.utils.IDataInput;
	import flash.utils.IDataOutput;
	import flash.utils.IExternalizable;
	import items.Ball;
	import items.BrickWall;
	import items.Domino;
	import items.FlintAndTinder;
	import items.Goal;
	import items.Rocket;
	import items.Wall;
	import mx.core.FlexSprite;
	import org.flixel.*;
	import org.flixel.plugin.photonstorm.FlxExtendedSprite;
	
	/**
	 * ...
	 * @author Ian Guthridge
	 */
	public class Item extends FlxSprite// implements IExternalizable
	{
		public static const SCALE_FACTOR:int = 1;
		public var fixture:b2Fixture;
		public var name:String;
		protected var m_simulate:Boolean;
		protected var m_drag:Boolean;
		protected var m_mouseOffset:Point;
		protected var m_rotateHandle:RotateHandle;
		protected var m_lockCheckBox:LockCheckBox;
		protected var m_locked:Boolean;
		
		//Special properties list
		protected var bActive:Boolean; 					//If the object has any special effects, they turn on when the object is "active."
		protected var bImmobile:Boolean;				//The item is static, not subject to the physics sim.
		protected var bTakesFlame:Boolean; 				//Turns Active when caught touched by fire.
		protected var bTakesBump:Boolean; 				//Turns on from generic bumps at any speed.
		protected var bTakesPress:Boolean; 				//Turns on from location-based presses, for plungers or switches.
		protected var bTakesElectricalWire:Boolean; 	//The object requires an active electrical connection.
		protected var bDestructible:Boolean;			//The object can be destroyed by explosives.
		
		//State properties
		protected var bOnFire:Boolean;
		
		public function Item(X:Number=0, Y:Number=0, SimpleGraphic:Class=null) 
		{
			super(X, Y, SimpleGraphic);	
			m_simulate = false;
			m_mouseOffset = new Point();
		}
		
		public function buildPhysics(world:b2World):void
		{
			
		}
		
		public function get locked():Boolean
		{
			return m_lockCheckBox.locked;
		}
		
		public function addListeners():void
		{
			FlxG.stage.addEventListener(MouseEvent.MOUSE_UP, onMouseUp);
			FlxG.stage.addEventListener(MouseEvent.MOUSE_DOWN, onMouseDown);
			m_lockCheckBox = new LockCheckBox(x + width - 12, y + 2);
			m_rotateHandle = new RotateHandle(rotateCallback, x + origin.x, y + origin.y, x + width + 10, y + height + 10);
		}
		
		protected function flxToB2d(p:FlxPoint):b2Vec2
		{
			var x:int = (p.x + origin.x) * SCALE_FACTOR;
			var y:int = (p.y + origin.y) * SCALE_FACTOR;
			return new b2Vec2(x, y);
		}
		
		protected function b2dToFlx(v:b2Vec2):FlxPoint
		{
			var x:int = (v.x / SCALE_FACTOR) - origin.x;
			var y:int = (v.y / SCALE_FACTOR) - origin.y;
			return new FlxPoint(x, y);
		}
		
		override public function update():void 
		{
			super.update();
			if (m_simulate)
			{
				_point = b2dToFlx(fixture.GetBody().GetPosition());				
				this.x = _point.x;
				this.y = _point.y;
				this.angle = FlxU.degFromRad(fixture.GetBody().GetAngle());
			}
			else if (m_drag)
			{
				var dx:Number = (FlxG.mouse.x - m_mouseOffset.x) - this.x;
				var dy:Number = (FlxG.mouse.y - m_mouseOffset.y) - this.y;
				this.x += dx;
				this.y += dy;
				if (m_rotateHandle)
				{
					this.m_rotateHandle.move(dx, dy);
				}
				if (m_lockCheckBox)
				{
					this.m_lockCheckBox.x += dx;
					this.m_lockCheckBox.y += dy;
				}
			}
			else if (m_rotateHandle)
			{
				m_rotateHandle.update();
			}
		}
		
		override public function draw():void 
		{
			super.draw();
			if (!m_simulate)
			{
				if( m_rotateHandle)
				{
					m_rotateHandle.draw();
				}
				if (m_lockCheckBox)
				{
					m_lockCheckBox.draw();
				}
			}
		}
		
		protected function rotateCallback(angle:Number):void
		{
			this.angle = angle;
		}
	
		
		protected function onMouseDown(event:MouseEvent):void
		{
			if (!m_simulate && active)
			{
				if (m_lockCheckBox && m_lockCheckBox.pixelsOverlapPoint(FlxG.mouse))
				{
					m_lockCheckBox.onMouseDown();
				}
				else if(this.pixelsOverlapPoint(FlxG.mouse))
				{
					m_drag = true;
					m_mouseOffset = new Point(FlxG.mouse.x - this.x, FlxG.mouse.y - this.y);
				}
				else if (m_rotateHandle)
				{
					m_rotateHandle.onMouseDown();
				}
			}
		}
		
		protected function onMouseUp(event:MouseEvent):void
		{
			if (!m_simulate && active)
			{
				m_drag = false;
				if (m_rotateHandle)
				{
					m_rotateHandle.onMouseUp();
				}
			}
		}
		
		public static function createItem(itemName:String, x:Number = 0, y:Number = 0, editable:Boolean = true):Item
		{
			var item:Item;
			switch(itemName)
			{
				case "tennisBall":					
					item = new Ball(x, y, getAsset(itemName), 12);					
					break;
				case "goal":
					item = new Goal(x, y);
					break;
				case "brick":
					item = new BrickWall(x, y, getAsset(itemName));
					break;			
				case "domino":
					item = new Domino(x, y, getAsset(itemName));
					break;
				case "flintAndTinder":
					item = new FlintAndTinder(x, y, getAsset(itemName));
					break;
				case "rocket":
					item = new Rocket(x, y, getAsset(itemName));
					break;
			}		
			if (item)
			{
				item.name = itemName;
				if(editable)
				{
					item.addListeners();
				}
			}
			return item;
		}
		
		public static function getAsset(itemName:String):Class
		{
			switch(itemName)
			{
				case "tennisBall":					
					return AssetsRegistry.tennisBallPNG;
				case "goal":
					return AssetsRegistry.goalButtonPNG;
				case "brick":
					return AssetsRegistry.brickWallPNG;
				case "domino":
					return AssetsRegistry.dominoPNG;
				case "flintAndTinder":
					return AssetsRegistry.flintLitPNG;
				case "rocket":
					return AssetsRegistry.rocketUnlitPNG;
			}
			return null;
		}
		
		public static function cloneItem(item:Item):Item
		{
			var clone:Item = createItem(item.name, item.x, item.y, false);
			clone.angle = item.angle;
			return clone;
		}
		
		public function toggleFire():void
		{
			this.bOnFire = !bOnFire;
		}
		
		public function toggleActive():void
		{
			this.bActive = !bActive;
		}
		
		public function get getFireState():Boolean
		{
			return bOnFire;
		}
	}

}