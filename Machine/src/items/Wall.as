package items
{
	import Box2D.Collision.Shapes.b2PolygonShape;
	import Box2D.Common.Math.b2Vec2;
	import Box2D.Dynamics.b2Body;
	import Box2D.Dynamics.b2BodyDef;
	import Box2D.Dynamics.b2FixtureDef;
	import Box2D.Dynamics.b2World;
	import org.flixel.*;
	/**
	 * ...
	 * @author Anders Howard
	 */
	public class Wall extends Item 
	{
		
		public function Wall(X:Number=0, Y:Number=0, SimpleGraphic:Class=null) 
		{
			
			super(X, Y, SimpleGraphic);
			this.makeGraphic(100, 25);
			m_rotateHandle = new RotateHandle(rotateCallback, x + origin.x, y + origin.y, x + width + 10, y + height + 10);
			
		}
		
		override public function buildPhysics(world:b2World):void 
		{
			var def:b2FixtureDef = new b2FixtureDef();
			def.shape = b2PolygonShape.AsBox(this.width / 2, this.height / 2);
			def.density = 100;
			var bodyDef:b2BodyDef = new b2BodyDef();
			bodyDef.position = flxToB2d(new FlxPoint(this.x, this.y));
			bodyDef.angle = FlxU.radFromDeg(this.angle);
			bodyDef.type = b2Body.b2_staticBody;
			var body:b2Body = world.CreateBody(bodyDef);
			fixture = body.CreateFixture(def);
			fixture.SetUserData(this);
			m_simulate = true;
		}
		
	}

}