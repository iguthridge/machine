package items
{
	import Box2D.Collision.Shapes.b2PolygonShape;
	import Box2D.Common.Math.b2Vec2;
	import Box2D.Dynamics.b2Body;
	import Box2D.Dynamics.b2BodyDef;
	import Box2D.Dynamics.b2ContactImpulse;
	import Box2D.Dynamics.b2FixtureDef;
	import Box2D.Dynamics.b2World;
	import Box2D.Dynamics.Contacts.b2Contact;
	import org.flixel.*;
	import org.flixel.plugin.photonstorm.FlxDelay;
	/**
	 * Flint and tinder
	 * @author Anders Howard
	 */
	public class FlintAndTinder extends Item 
	{
		private var bContacted:Boolean = false;
		private var m_delay:FlxDelay = new FlxDelay(200);
		
		public function FlintAndTinder(X:Number=0, Y:Number=0, SimpleGraphic:Class=null) 
		{
			super(X, Y, SimpleGraphic);
			
			this.loadGraphic(AssetsRegistry.flintUnlitPNG);
			
			//Busted ass animation junk.
			/*this.loadGraphic(AssetsRegistry.flintSheetPNG, true, false, 64, 64);
			this.addAnimation("strike", [0, 1, 2, 1, 0], 10, false);
			this.addAnimation("burn", [3], 1, false);
			this.addAnimationCallback(animationChange);*/
			//Set up delay callback.
			//m_delay.callback = playBurn;
			
		}
		
		
		private function animationChange(name:String, frameNum:uint, frameIndex:uint):void
		{
			m_delay.start();
		}
		
		private function playBurn():void
		{
			this.play("burn");
		}
		
		override public function buildPhysics(world:b2World):void 
		{
			var def:b2FixtureDef = new b2FixtureDef();
			def.shape = b2PolygonShape.AsBox(this.width / 4, this.height / 4);
			def.density = 100;
			var bodyDef:b2BodyDef = new b2BodyDef();
			bodyDef.position = flxToB2d(new FlxPoint(this.x, this.y));
			bodyDef.angle = FlxU.radFromDeg(this.angle);
			bodyDef.type = b2Body.b2_staticBody;
			var body:b2Body = world.CreateBody(bodyDef);
			fixture = body.CreateFixture(def);
			fixture.SetUserData(this);
			m_simulate = true;
		}
		
		public function beginContact(contact:b2Contact):void
		{
			//At contact do some stuff.
			if (bContacted == false)
			{
				bContacted = true;
				if (this.bOnFire == false)
				{
					this.toggleFire();
				}
				this.loadGraphic(AssetsRegistry.flintLitPNG);
			}
			
		}
		
		public function postSolve(postSolveContact:b2Contact, postSolveImpulse:b2ContactImpulse):void
		{
			//After contact do some stuff.
			/*if (postSolveContact.GetFixtureA.data == this)
			{
				//Then check fixture b 
			}*/
		}
		
	}

}