package items
{
	import Box2D.Collision.b2Point;
	import Box2D.Collision.Shapes.b2PolygonShape;
	import Box2D.Common.Math.b2Vec2;
	import Box2D.Dynamics.b2Body;
	import Box2D.Dynamics.b2BodyDef;
	import Box2D.Dynamics.b2ContactImpulse;
	import Box2D.Dynamics.b2FixtureDef;
	import Box2D.Dynamics.b2World;
	import Box2D.Dynamics.Contacts.b2Contact;
	import org.flixel.*;
	import org.flixel.plugin.photonstorm.FlxDelay;
	/**
	 * Rocket: ignites after touching fire and then applies force impulse.
	 * @author Anders Howard
	 */
	public class Rocket extends Item 
	{
		private var bContacted:Boolean = false;
		private var m_delay:FlxDelay = new FlxDelay(500);
		private var m_bodyRef:b2BodyDef;
		
		public function Rocket(X:Number=0, Y:Number=0, SimpleGraphic:Class=null) 
		{
			super(X, Y, SimpleGraphic);
			this.bTakesFlame = true;	//Doesn't actually do anything at the moment.
			m_delay.callback = launchRocket;
			
			this.loadGraphic(AssetsRegistry.rocketUnlitPNG);
		}
		
		override public function buildPhysics(world:b2World):void 
		{
			var def:b2FixtureDef = new b2FixtureDef();
			def.shape = b2PolygonShape.AsBox(this.width / 4, this.height / 4);
			def.density = 100;
			var bodyDef:b2BodyDef = new b2BodyDef();
			bodyDef.position = flxToB2d(new FlxPoint(this.x, this.y));
			bodyDef.angle = FlxU.radFromDeg(this.angle);
			bodyDef.type = b2Body.b2_dynamicBody;
			m_bodyRef = bodyDef;
			var body:b2Body = world.CreateBody(bodyDef);
			fixture = body.CreateFixture(def);
			fixture.SetUserData(this);
			m_simulate = true;
		}
		
		public function beginContact(contact:b2Contact):void
		{
			/*
			//At contact do some stuff.
			if (bContacted == false)
			{
				var classA:Item = contact.GetFixtureA().GetUserData();
				var classB:Item = contact.GetFixtureB().GetUserData();
				bContacted = true;
				
				if (classA != this)
				{
					if (classA.getFireState==true)
					{
						m_delay.start();
					}
				}
				
				else if (classB != this)
				{
					if (classB.getFireState==true)
					{
						m_delay.start();
					}
				}
			}*/
			
		}
		
		public function postSolve(contact:b2Contact, impulse:b2ContactImpulse):void
		{
			//At contact do some stuff.
			if (bContacted == false)
			{
				var classA:Item = contact.GetFixtureA().GetUserData();
				var classB:Item = contact.GetFixtureB().GetUserData();
				bContacted = true;
				
				if (classA != null && classB != null)
				{
					if (classA != this)
						{
							if (classA.getFireState==true)
							{
								m_delay.start();
							}
						}
						
						else if (classB != this)
						{
							if (classB.getFireState==true)
							{
								m_delay.start();
							}
						}
				}
				
			}
		}
		
		override public function update():void 
		{
			super.update();
			if (bOnFire)
			{
				var direction:FlxPoint = FlxU.rotatePoint(0, 1, 0, 0, FlxU.degFromRad(this.fixture.GetBody().GetAngle()));
				var forceVector:b2Vec2 = new b2Vec2(direction.x, direction.y);
				forceVector.Multiply(1000000000);
				var tempBody:b2Body = this.fixture.GetBody();
				tempBody.ApplyForce(forceVector, this.fixture.GetBody().GetPosition());
			}
		}
		
		private function launchRocket():void
		{		
			//Swap anim
			this.loadGraphic(AssetsRegistry.rocketLitPNG);
			
			//Get impulse force vector and direction vector, then apply impulse.
			bOnFire = true;
		}
		
	}

}