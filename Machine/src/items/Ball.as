package items
{
	import Box2D.Collision.Shapes.b2CircleShape;
	import Box2D.Common.Math.b2Vec2;
	import Box2D.Dynamics.b2Body;
	import Box2D.Dynamics.b2BodyDef;
	import Box2D.Dynamics.b2Fixture;
	import Box2D.Dynamics.b2FixtureDef;
	import Box2D.Dynamics.b2World;
	import flash.events.MouseEvent;
	import flash.geom.Point;
	import org.flixel.*;
	/**
	 * ...
	 * @author Ian Guthridge
	 */
	public class Ball extends Item 
	{
		protected var m_radius:int;
		
		public function Ball(X:Number=0, Y:Number=0, SimpleGraphic:Class = null, radius:int = 10) 
		{
			
			super(X, Y, SimpleGraphic);
			m_radius = radius;
			
		}
		
		override public function addListeners():void 
		{
			super.addListeners();
			m_rotateHandle = null;
		}

		override public function buildPhysics(world:b2World):void 
		{
			var def:b2FixtureDef = new b2FixtureDef();
			def.shape = new b2CircleShape(m_radius);
			def.density = 100;
			def.restitution = .9;
			var bodyDef:b2BodyDef = new b2BodyDef();
			bodyDef.position = flxToB2d(new FlxPoint(this.x, this.y));
			bodyDef.angle = FlxU.radFromDeg(this.angle);
			bodyDef.type = b2Body.b2_dynamicBody;
			var body:b2Body = world.CreateBody(bodyDef);
			fixture = body.CreateFixture(def);
			fixture.SetUserData(this);
			m_simulate = true;
		}
		
	}

}