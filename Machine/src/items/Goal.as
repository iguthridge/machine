package  items
{
	import Box2D.Collision.Shapes.b2CircleShape;
	import Box2D.Collision.Shapes.b2Shape;
	import Box2D.Dynamics.b2Body;
	import Box2D.Dynamics.b2BodyDef;
	import Box2D.Dynamics.b2FixtureDef;
	import Box2D.Dynamics.b2World;
	import Box2D.Dynamics.Contacts.b2Contact;
	import org.flixel.*;
	/**
	 * ...
	 * @author Ian Guthridge
	 */
	public class Goal extends Item 
	{
		

		public var completeCallback:Function;
		public function Goal(X:Number=0, Y:Number=0) 
		{
			super(X, Y, AssetsRegistry.goalButtonPNG);			
		}
		
		override public function addListeners():void 
		{
			super.addListeners();
			m_rotateHandle = null;
			m_lockCheckBox = null;
		}
		
		override public function get locked():Boolean
		{
			return true;
		}
		
		override public function buildPhysics(world:b2World):void 
		{
			var def:b2FixtureDef = new b2FixtureDef();
			def.shape =  new b2CircleShape(width / 2);
			def.density = 100;
			def.restitution = .9;
			var bodyDef:b2BodyDef = new b2BodyDef();
			bodyDef.position = flxToB2d(new FlxPoint(this.x, this.y));
			bodyDef.angle = FlxU.radFromDeg(this.angle);
			bodyDef.type = b2Body.b2_staticBody;
			var body:b2Body = world.CreateBody(bodyDef);
			fixture = body.CreateFixture(def);
			fixture.SetUserData(this);
			m_simulate = true;
		}
		
		public function beginContact(contact:b2Contact):void
		{
			if (FlxG.state is PlayState)
			{
				completeCallback();
			}
		}
		
	}

}