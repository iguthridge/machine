package
{
	import flash.net.registerClassAlias;
	import items.Ball;
	import items.BrickWall;
	import items.Domino;
	import items.Goal;
	import items.Rocket;
	import items.Wall;
	import org.flixel.*;
 
	public class LevelsBuilt
	{
		private static var _save:FlxSave; //The FlxSave instance
		private static var _temp:Object;
		private static var _loaded:Boolean = false; //Did bind() work? Do we have a valid SharedObject?
		
		/**
		 * Returns the number of level names
		 */
		public static function get levels():Array
		{
			//We only get data from _save if it was loaded properly. Otherwise, use _temp
			if (_loaded)
			{
				return _save.data.levelNames;
			}
			else
			{
				return _temp ? [] : _temp.levelNames;
			}
		}
		
		/**
		 * Sets the number of levels that the player has completed
		 */
		public static function addLevel(name:String, data:LevelData):void
		{
			if (_loaded)
			{
				_save.data.levelNames.push(name);
				_save.data.levels[name] = data;
			}
			else
			{
				if (!_temp)
				{
					_temp = { levelNames:[], levels: { }};
				}
				_temp.levelNames.push(name);
				_temp.levels[name] = data;
			}
		}
		
		public static function getLevel(name:String):LevelData
		{
			if (_loaded)
			{
				return _save.data.levels[name];
			}
			else
			{
				if (!_temp)
				{
					_temp = { levelNames:[], levels: { }};
				}
				return _temp.levels[name];
			}
		}
		
 
		/**
		 * Setup LevelsCompleted
		 */
		public static function load():void
		{
			if (!_loaded)
			{
				registerClassAlias("LevelData", LevelData);
				registerClassAlias("Item", Item);
				registerClassAlias("items.BrickWall", BrickWall);
				registerClassAlias("items.Ball", Ball);
				registerClassAlias("items.Goal", Goal);
				registerClassAlias("items.Domino", Domino);
				registerClassAlias("items.Rocket", Rocket);
				_save = new FlxSave();
				_loaded = _save.bind("userLevelData");
				if (_loaded && _save.data.levels == null)
				{
					if (_temp)
					{
						_save.data = _temp;
					}
					else
					{
						_save.data.levelNames = [];
						_save.data.levels = {};
					}
				}
			}
		}
	}
}