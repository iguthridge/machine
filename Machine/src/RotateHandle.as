package  
{
	import Box2D.Common.Math.b2Vec2;
	import flash.geom.Point;
	import org.flixel.*;
	
	/**
	 * ...
	 * @author Ian Guthridge
	 */
	public class RotateHandle extends FlxSprite 
	{
		protected var m_drag:Boolean;
		protected var m_moveCallback:Function;
		protected var m_mouseOffset:Point;
		protected var m_origin:FlxPoint;
		protected var m_initialLength:Number;
		protected var m_initialAngle:Number;
		public function RotateHandle(moveCallback:Function, oX:Number, oY:Number, X:Number, Y:Number) 
		{
			super(X, Y, AssetsRegistry.rotatorArrowPNG);
			//makeGraphic(10, 10);
			m_moveCallback = moveCallback;
			m_origin = new FlxPoint(oX, oY);
			var vec:b2Vec2 = new b2Vec2(x + origin.x - m_origin.x, y + origin.y - m_origin.y);
			m_initialLength = vec.Length();
			m_initialAngle = FlxU.degFromRad(Math.atan2(y + origin.x - m_origin.y, x + origin.y - m_origin.x));
		}
		
		override public function update():void 
		{
			super.update();
			if (m_drag)
			{
				this.x = FlxG.mouse.x - m_mouseOffset.x;
				this.y = FlxG.mouse.y - m_mouseOffset.y;
				
				var vec:b2Vec2 = new b2Vec2(x + origin.x - m_origin.x, y + origin.y - m_origin.y);
				
				if (vec.Length() != m_initialLength)
				{
					vec.Normalize();
					vec.Multiply(m_initialLength);
					this.x = vec.x + m_origin.x - origin.x;
					this.y = vec.y + m_origin.y - origin.y;
				}
				
				var angle:Number = FlxU.degFromRad(Math.atan2(y + origin.x - m_origin.y, x + origin.y - m_origin.x));
				m_moveCallback(angle - m_initialAngle);
			}
		}
		
		public function onMouseDown():void
		{
			if (this.pixelsOverlapPoint(FlxG.mouse) || this.overlapsPoint(FlxG.mouse, true))
			{
				m_drag = true;
				m_mouseOffset = new Point(FlxG.mouse.x - this.x, FlxG.mouse.y - this.y);
			}
		}
		
		public function onMouseUp():void
		{
			m_drag = false;
		}
		
		public function move(dx:Number, dy:Number):void 
		{
			m_origin.x += dx;
			m_origin.y += dy;
			x += dx;
			y += dy;
		}
		
	}

}