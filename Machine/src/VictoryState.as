package
{

	import org.flixel.*;

	public class VictoryState extends FlxState
	{
		private static var s_levelCount:int = 0;
		static public var fromFriend:Boolean;
		private var playButton:FlxButton;
		private var devButton:FlxButton;
		private var Title:FlxText;
		protected var m_initialItems:Array;
		protected var m_testMode:Boolean
		protected var m_fromInv:Array;
		
		public function VictoryState(testMode:Boolean, initialItems:Array, fromInv:Array)
		{
			m_testMode = testMode;
			m_initialItems = initialItems;
			m_fromInv = fromInv;
		}
		
		override public function create():void
		{
			FlxG.bgColor = 0xff000000;
			
			Title = new FlxText(FlxG.width / 2, FlxG.height / 3, 100, m_testMode ? "Looks like your puzzle works" : "A winner is you")
			Title.alignment = "center";
			add(Title);
			
			if (m_testMode || fromFriend)
			{
				devButton = new FlxButton(FlxG.width/2-40,FlxG.height / 3 + 60, m_testMode ? "Tweak it?" : "Make a Puzzle to send back.", onSite);
				devButton.soundOver = null;  //replace with mouseOver sound
				devButton.color = 0xffD4D943;
				devButton.label.color = 0xffD8EBA2;
				add(devButton);
			}
			
			if (!fromFriend)
			{
				add(new FlxSprite(100, 50, AssetsRegistry.winnerDialogPNG));
				InventoryManager.gotRocket = true;
			}
			
			playButton = new FlxButton(FlxG.width/2-40,FlxG.height / 3 + 100, m_testMode ? "Send to friend" : "menu", onPlay);
			playButton.soundOver = null;
			playButton.color  = 0xffD4D943;
			playButton.label.color= 0xffD8EBA2;
			add(playButton);
			
			
			FlxG.mouse.show();
			
		}
		
		override public function update():void
		{
			super.update();	
		}
		
		protected function onSite():void
		{
			if (m_testMode)
			{
				FlxG.switchState(new ConstructState(m_testMode, m_initialItems, m_fromInv));
			}
			else
			{
				//FlxG.loadReplay(PlayState.save, new PlayState(null, null, null, false), ["ANY", "MOUSE"], 0, replayDone);
				FlxG.switchState(new MenuState());
			}
		}
			
		/*public function replayDone():void
		{
			FlxG.switchState(new MenuState());
		}*/
		
		protected function onPlay():void
		{
			if (m_testMode)
			{
				var levelData:LevelData = new LevelData();
				for each (var item:Item in m_initialItems)
				{
					if (item.locked)
					{
						levelData.world.push(item);
					}
					else 
					{
						levelData.inventory.push(item.name);
					}
				}
				LevelsBuilt.load();
				LevelsBuilt.addLevel("Anders's level", levelData);	
				FlxG.switchState(new MenuState());
			}
			else
			{
				FlxG.switchState(new MenuState());
			}
		}
		
		
	}
}

