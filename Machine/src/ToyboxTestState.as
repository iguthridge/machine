package
{

	import Box2D.Common.Math.b2Vec2;
	import Box2D.Dynamics.b2DebugDraw;
	import Box2D.Dynamics.b2World;
	import flash.display.Sprite;
	import org.flixel.*;
	
	public class ToyboxTestState extends FlxState
	{
		public var world:b2World;
		protected var m_items:FlxGroup;
		protected var m_unplaced:Array;
		protected var debug:b2DebugDraw;
		
		protected var m_hudGroup:FlxGroup;
		protected var m_toybox:Toybox;
		protected var m_inventoryManager:InventoryManager;
		
		public function ToyboxTestState(items:Array)
		{
			m_unplaced = items;
			debug = new b2DebugDraw();
			var s:Sprite = new Sprite();
			FlxG.stage.addChild(s);
			debug.SetSprite(s);
			debug.SetDrawScale(1);// 30.0;
			debug.SetFillAlpha(0.3);
			debug.SetLineThickness(1.0);
			debug.SetFlags(b2DebugDraw.e_shapeBit | b2DebugDraw.e_jointBit);
			
			//load toybox
			m_inventoryManager = new InventoryManager;
			trace("Adding toybox.");
			m_toybox = new Toybox(Toybox.MODE_LEVEL, m_inventoryManager);
			add(m_toybox);
		}
		
		override public function create():void
		{
			world = new b2World(new b2Vec2(0, 98.1), true);
			world.SetDebugDraw(debug);
			m_items = new FlxGroup();
			placeItems(m_unplaced);
			m_unplaced = null;
		}
				
		public function placeItems(items:Array):void
		{
			for each(var item:Item in items)
			{
				item.buildPhysics(world);
				m_items.add(item);
			}
			this.add(m_items);
		}
		
		override public function update():void
		{
			var elapsed:Number = FlxG.elapsed;
			super.update();
			m_items.update();
			world.Step(elapsed, 10, 10);
			world.DrawDebugData();
		}
	}
}

