package
{

	import flash.events.Event;
	import flash.net.URLLoader;
	import flash.net.URLRequest;
	import org.flixel.*;
	import org.flixel.plugin.photonstorm.FlxButtonPlus;

	public class LevelSelectState extends FlxState
	{
		private var m_Title:FlxText;
		private var m_inventoryManager:InventoryManager
		private var m_numberOfDevLevels:int;
		private var m_levelButtonsArray:Array = new Array();
		private var m_levelXML:XML;
		
		private const LEVEL_BUTTON_SPACING:int = 60;
		private const LEVEL_BUTTON_HEIGHT:int = 75;
		private const LEVEL_BUTTON_WIDTH:int = 100;
		private const LEVEL_BUTTON_VERTICAL_SPACING:int = 200;
		
		public static var currentWorld:Array;
		
		override public function create():void
		{
			m_inventoryManager = new InventoryManager;
			LevelSelectState.currentWorld = null;
			InventoryManager.levelInventory = null;
			FlxG.bgColor = 0xff000000;
			
			m_Title = new FlxText(250, 20, 150, "Level Select")
			m_Title.alignment = "center";
			m_Title.size = 20;
			add(m_Title);
			
			getNumberOfLevelsFromXML();						//Plot out and display all developer made levels as buttons.
			getSavedPlayerConstructedLevels();
			
			FlxG.mouse.show();
		}
		
		private function getSavedPlayerConstructedLevels():void 
		{
			LevelsBuilt.load();
			var levels:Array = LevelsBuilt.levels;
			for (var i:int = 0; i < levels.length; ++i)
			{
				var levelButton:FlxButtonPlus = new FlxButtonPlus(LEVEL_BUTTON_SPACING, LEVEL_BUTTON_VERTICAL_SPACING * 1.5, onPlayerLevelSelected, [LevelsBuilt.getLevel(levels[i])], levels[i], LEVEL_BUTTON_WIDTH, LEVEL_BUTTON_HEIGHT);
				m_levelButtonsArray.push(levelButton);
				add(levelButton);
			}
		}
		
		override public function update():void
		{
			super.update();	
		}		
		
		//For levels made by devs, plot them in their own section separate from player levels. 
		private function getNumberOfLevelsFromXML():void
		{
			var levelUrlLoader:URLLoader = new URLLoader();
			levelUrlLoader.load(new URLRequest("../src/data/levels.xml"));
			levelUrlLoader.addEventListener(Event.COMPLETE, processLevelXML);
		}
		
		private function processLevelXML(e:Event):void
		{
			m_levelXML = new XML(e.target.data);
			
			for (var i:Number = 0; i < m_levelXML.level.length(); i++)
			{
				m_numberOfDevLevels++;
			}
			trace("Number of levels described in XML: " + m_numberOfDevLevels);
			plotDevLevelButtons();
		}
		
		private function plotDevLevelButtons():void
		{
			for (var i:int = 0; i < m_numberOfDevLevels; i++)
			{
				//var paramArray:Array = new Array(i);
				var levelButton:FlxButtonPlus = new FlxButtonPlus(LEVEL_BUTTON_SPACING, LEVEL_BUTTON_VERTICAL_SPACING, onLevelSelected, [i], "Level " + (i + 1), LEVEL_BUTTON_WIDTH, LEVEL_BUTTON_HEIGHT);
				m_levelButtonsArray.push(levelButton);
				add(levelButton);
			}
		}
		
		protected function onLevelSelected(levelNum:int):void
		{
			trace("Level " + (levelNum + 1) + " selected.");
			m_levelButtonsArray = null;
			var level:XML = m_levelXML.level[0];			
			var vector:Vector.<String> = new Vector.<String>();
			for each(var item:XML in level.inventory.item)
			{				
				vector.push(item[0]);
			}
			InventoryManager.levelInventory = vector;
			var world:Array = [];
			for each(var entry:XML in level.world.item)
			{
				var name:String = entry.@name[0];
				var x:Number = entry.@x[0];
				var y:Number = entry.@y[0];
				var prototype:Item = Item.createItem(name, x, y, false);
				prototype.angle = entry.@angle[0];
				world.push(prototype);
			}
			InventoryManager.levelInventory = vector;		
			currentWorld = world;
			FlxG.switchState(new ConstructState(false));
			VictoryState.fromFriend = false;
		}
		
		protected function onPlayerLevelSelected(levelData:LevelData):void
		{
			m_levelButtonsArray = null;
			var vector:Vector.<String> = new Vector.<String>();
			for each(var name:String in levelData.inventory)
			{
				vector.push(name);
			}
			InventoryManager.levelInventory = vector;		
			currentWorld = levelData.world;
			FlxG.switchState(new ConstructState(false));
			VictoryState.fromFriend =true;
		}
		
	}
}

