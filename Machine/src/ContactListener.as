package  
{
	import Box2D.Collision.b2Manifold;
	import Box2D.Dynamics.b2ContactImpulse;
	import Box2D.Dynamics.b2ContactListener;
	import Box2D.Dynamics.Contacts.b2Contact;
	/**
	 * ...
	 * @author Ian Guthridge
	 */
	public class ContactListener extends b2ContactListener 
	{
		
		public function ContactListener() 
		{
			
		}		

		public override function BeginContact(contact:b2Contact):void 
		{ 
			var data:* = contact.GetFixtureA().GetUserData();
			if (data && data.hasOwnProperty('beginContact'))
			{
				data.beginContact(contact);
			}
			data = contact.GetFixtureB().GetUserData();
			if (data && data.hasOwnProperty('beginContact'))
			{
				data.beginContact(contact);
			}
		}

		public override function EndContact(contact:b2Contact):void
		{ 
			var data:* = contact.GetFixtureA().GetUserData();
			if (data && data.hasOwnProperty('endContact'))
			{
				data.endContact(contact);
			}
			data = contact.GetFixtureB().GetUserData();
			if (data && data.hasOwnProperty('endContact'))
			{
				data.endContact(contact);
			}
		}
		public override function PreSolve(contact:b2Contact, oldManifold:b2Manifold):void
		{ 
			
		}

		public override function PostSolve(contact:b2Contact, impulse:b2ContactImpulse):void
		{ 
			var data:* = contact.GetFixtureA().GetUserData();
			if (data && data.hasOwnProperty('postSolve'))
			{
				data.postSolve(contact, impulse);
			}
			data = contact.GetFixtureB().GetUserData();
			if (data && data.hasOwnProperty('postSolve'))
			{
				data.postSolve(contact, impulse);
			}
		}
		
	}

}