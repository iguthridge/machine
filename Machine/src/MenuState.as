package
{

	import org.flixel.*;
	import org.flixel.plugin.photonstorm.FlxButtonPlus;

	public class MenuState extends FlxState
	{
		
		private var constructButton:FlxButtonPlus;
		private var playButton:FlxButtonPlus;
		private var Title:FlxText;
		
		override public function create():void
		{
			FlxG.bgColor = 0xff000000;
			LevelSelectState.currentWorld = null;
			InventoryManager.levelInventory = null;
			
			Title = new FlxText(FlxG.width / 2, FlxG.height / 3, 150, "Machine")
			Title.alignment = "center";
			Title.size = 20;
			add(Title);
			
			constructButton = new FlxButtonPlus(FlxG.width / 2 + 25, FlxG.height / 3 + 50, onConstruct, null, "Construction Mode", 100, 50); 
			//(FlxG.width/2-40,FlxG.height / 3 + 100, "Click To Play", onPlay);
			//playButton.onColor.push(0xffD4D943);
			constructButton.borderColor = 111111;
			//playButton.label.color = 0xffD8EBA2;
			//playButton.label.size = 12;
			add(constructButton);
			
			playButton = new FlxButtonPlus(FlxG.width / 2 + 25, FlxG.height / 3 + 125, onPlay, null, "Play Mode", 100, 50); 
			playButton.borderColor = 333333;
			add(playButton);
			
			FlxG.mouse.show();
		}
		
		override public function update():void
		{
			super.update();	
		}
		
		protected function onConstruct():void
		{
			constructButton.exists = false;
			FlxG.switchState(new ConstructState(true));
		}
		
		protected function onPlay():void
		{
			playButton.exists = false;
			FlxG.switchState(new LevelSelectState());
		}
		
		
	}
}

