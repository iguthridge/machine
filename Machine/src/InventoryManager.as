package  
{
	import flash.events.Event;
	import flash.net.URLLoader;
	import flash.net.URLRequest;
	import org.flixel.FlxSave;
	/**
	 * Holds references to the player inventory and also accesses level data to load level inventories.
	 * @author Anders Howard
	 */
	public class InventoryManager 
	{
		private var urlLoader:URLLoader;
		private var m_playerInvSave:FlxSave;
		private var m_levelXML:XML;
		private var m_levelNumber:int;
		private var m_levelInventory:Vector.<String>;
		private var m_playerInventory:Vector.<String>;
		private var initialItemsGranted:Boolean = false;
		
		
		public static var levelInventory:Vector.<String>;
		
		public static var gotRocket:Boolean = false;
		
		public function InventoryManager() 
		{
			m_playerInventory = new Vector.<String>;
			initialPlayerInventory();
		}
		
		private function initialPlayerInventory():void
		{
			if (!initialItemsGranted)
			{
				m_playerInventory.push("brick", "tennisBall", "domino", "flintAndTinder");
			}
			if (gotRocket)
			{
				m_playerInventory.push("rocket");
			}
			initialItemsGranted = true;
		}
		
		public function getPlayerInventory():Vector.<String>
		{
			return m_playerInventory;
		}
		
		public function addItemToPlayerInventory(itemName:String):void
		{
			m_playerInventory.push(itemName);
		}
		
		public function getLevelInventory(levelNum:int):Vector.<String>
		{
			var returnVal:Vector.<String> 
			if (levelInventory != null)
			{
				returnVal = levelInventory;
			}
			else
			{
			
				m_levelNumber = levelNum;
				urlLoader = new URLLoader(new URLRequest("../src/data/levels.xml"));
				urlLoader.addEventListener(Event.COMPLETE, processLevelXML);
				returnVal = returnLevelInventory();
			}
			return returnVal;
		}
		
		public function resetLevelInventory():void
		{
			m_levelInventory = null;
		}
		
		// For each item in the level data asked for, grab the name and shove it into an array. 
		private function processLevelXML(e:Event):void
		{
			m_levelInventory = new Vector.<String>;
			m_levelXML = new XML(e.target.data);
			trace(m_levelXML.level[m_levelNumber-1]);
			
			for (var i:Number = 0; i < m_levelXML.level[m_levelNumber-1].inventory.item.length() ; i++)
			{
				m_levelInventory.push(m_levelXML.level[m_levelNumber - 1].inventory.item[i]);
			}
			trace("Contents of the current level inventory: " + m_levelInventory);
		}
		
		private function returnLevelInventory():Vector.<String>
		{
			return m_levelInventory;
		}
		
	}

}